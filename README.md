# Description
A simple GTK app for controlling my gates remotely.

It communicates with an Arduino Opta PLC running the GateMeisterController firmware.
The controller must be connected to the gate control signals.


# Dependencies
sudo apt install meson libgtk-4-dev libmosquitto-dev libssl-dev libmodbus-dev


# Build instructions

```bash
meson setup build
cd build
meson compile
meson install
```

