#include "portscan.h"

#include <time.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <poll.h>

#define portscan_err(...) portscan_syslog(PORTSCAN_LOG_LEVEL_ERROR, __VA_ARGS__)
#define portscan_log(...) portscan_syslog(PORTSCAN_LOG_LEVEL_INFO,  __VA_ARGS__)
#define portscan_dbg(...) portscan_syslog(PORTSCAN_LOG_LEVEL_DEBUG, __VA_ARGS__)

static portscan_log_level_t g_log_level = PORTSCAN_LOG_LEVEL_ERROR;

static unsigned g_sock_wait_tm = 1500;

void portscan_syslog(portscan_log_level_t level, const char *fmt, ...)
{
	if ( level <= g_log_level )
	{
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}

int portscan_set_log_level(portscan_log_level_t level)
{
	if ( level < PORTSCAN_LOG_LEVEL_COUNT )
	{
		g_log_level = level;
		return 0;
	}

	return -1;
}

int portscan_set_sock_wait_tm(unsigned wait_ms)
{
	if ( wait_ms > 100 && wait_ms < 9000 )
	{
		g_sock_wait_tm = wait_ms;
		return 0;
	}

	return -1;
}

void portscan_sleep_ms(unsigned count)
{
	struct timespec rem = {0};
	struct timespec req= { count / 1000, (count % 1000) * 1000 * 1000 };
	nanosleep(&req , &rem);
}

/*
 * Block until fd becomes writable or timeout expires
 *
 * Returns:
 *      0 on timeout
 *     >0 if data is available
 *     -1 on error
 */
int portscan_wait_for_writable(int fd, int timeout_ms)
{
	struct pollfd fds[2];

	fds[0].fd = fd;
	fds[0].events = POLLOUT;

	return poll(fds, 1, timeout_ms);
}

/*
 * Create a non-blocking socket and initiate the connection process.
 * Returns -1 on error or a valid socket descriptor on success
 */
int socket_nonblock_connect(char* addr, unsigned port)
{
	/* create socket */
	int sock_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

	if (sock_fd == -1)
	{
		portscan_err("socket creation failed, errno:%d\n", errno);
		return -1;
	}

	/* setup server address */
	struct sockaddr_in serv_addr = {0};
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(addr);
	serv_addr.sin_port = htons(port);

	/* connect to server */
	int status = connect(sock_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	if ( status == -1 && errno == EINPROGRESS )
	{
		return sock_fd;
	}

	portscan_err("connect() failed with status:%d errno:%d\n", status, errno);
	close(sock_fd);
	return -1;
}

/*
 * Verify the socket has connected.
 */
int socket_nonblock_validate(int sock_fd)
{
	int error = 0;
	socklen_t len = sizeof(int);
	int status = getsockopt(sock_fd, SOL_SOCKET, SO_ERROR, &error, &len);

	if ( status != 0 || error != 0 )
	{
		if ( error == ECONNREFUSED || error == EHOSTUNREACH )
			portscan_dbg("socket connect failed, status:%d so-error:%d\n", status, error);
		else
			portscan_err("socket connect failed, status:%d so-error:%d\n", status, error);
		close(sock_fd);
		return -1;
	}

	return sock_fd;
}

int is_local_ip(char* p_addr)
{
	return strncmp(p_addr, "192.168", 7) == 0;
}

/*
 * Tries to find a suitable ip address on the device we are running on.
 * Returns 0 and copies the address to p_out on success
 */
int find_our_ip_addr(char *p_addr)
{
	int status = -1;

	struct ifaddrs *ifaddr;

	if ( getifaddrs(&ifaddr) == -1 )
	{
		portscan_err("getifaddrs() failed with errno %d", errno);
		return -1;
	}

	/* check each network interface for a suitable ip */
	for (struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
	{
		if (ifa->ifa_addr == NULL)
			continue;

		int family = ifa->ifa_addr->sa_family;

		if( family == AF_INET || family == AF_INET6 )
		{
			char host[NI_MAXHOST];
			int error = getnameinfo(ifa->ifa_addr,
				(family == AF_INET) ?
					sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6),
				host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

			if( error )
			{
				portscan_err("getnameinfo() failed: %s\n", gai_strerror(error));
				break;
			}

			if( is_local_ip(host) )
			{
				strcpy(p_addr, host);
				status = 0;
				break;
			}
		}
	}

	freeifaddrs(ifaddr);

	return status;
}

/*
 * Finds the range (network prefix) of the passed ip address
 * e.g. for 192.168.0.1 we would copy "192.168.0." to p_range
 */
int find_ip_range(char *p_addr, char *p_range)
{
	int status = -1;

	unsigned dots_found = 0;

	for(int i=0; i<12; i++)
	{
		p_range[i] = p_addr[i];

		if( p_addr[i] == '.' )
			dots_found++;

		if ( dots_found == 3 )
		{
			p_range[i+1] = '\0';
			status = 0;
			break;
		}
	}

	return status;
}

int portscan(struct portscan_data *data, unsigned port)
{
	int *sock_fds = data->sock_fds;

	int status = find_our_ip_addr(data->ip_addr);
	if ( status )
	{
		portscan_err("Failed to find client address with error %d\n", status);
		return -1;
	}
	else
	{
		portscan_log("Detected IP address %s\n", data->ip_addr);
	}

	status = find_ip_range(data->ip_addr, data->ip_range);
	if ( status )
	{
		portscan_err("Failed to find address range with error %d\n", status);
		return -1;
	}
	else
	{
		portscan_log("Detected IP range %s\n", data->ip_range);
	}

	/* initiate a connection to each possible address */
	for(int i=0; i<256; i++)
	{
		sock_fds[i] = -1;

		if ( i == 0 || i == 255 )
			continue;

		char tmp_addr[20];
		sprintf(tmp_addr, "%s%d", data->ip_range, i);

		sock_fds[i] = socket_nonblock_connect(tmp_addr, port);
	}

	portscan_dbg("waiting for sockets to become writable\n");
	portscan_sleep_ms(g_sock_wait_tm);

	portscan_dbg("checking if connections have succeeded\n");
	for(int i=0; i<256; i++)
	{
		if ( sock_fds[i] >= 0 )
		{
			int status = portscan_wait_for_writable(sock_fds[i], 0);
			if ( status <= 0 )
			{
				close( sock_fds[i] );
				sock_fds[i] = -1;
			}
			else
			{
				portscan_dbg("sock_fd[%d] is writable\n", i);
			}
		}
	}

	for(int i=0; i<256; i++)
	{
		if ( sock_fds[i] >= 0 )
		{
			portscan_dbg("checking sock_fd[%d]\n", i);
			sock_fds[i] = socket_nonblock_validate( sock_fds[i] );

			if ( sock_fds[i] >= 0 )
			{
				portscan_log("Found open port at address %d\n", i);
			}
		}
	}

	return 0;
}

