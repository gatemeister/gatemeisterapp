/*
 * Copyright (C) 2023
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This program implements a scanner for finding open ports on the local network
 */
#pragma once

typedef enum
{
	PORTSCAN_LOG_LEVEL_NONE = 0,
	PORTSCAN_LOG_LEVEL_ERROR,
	PORTSCAN_LOG_LEVEL_INFO,
	PORTSCAN_LOG_LEVEL_DEBUG,
	PORTSCAN_LOG_LEVEL_COUNT
} portscan_log_level_t;

struct portscan_data
{
	char ip_addr[16];
	char ip_range[16];
	int sock_fds[256];
};

/*
 * Detects local ip address, and opens a socket to
 * all devices which have the specified port open.
 *
 * Returns 0 on success
 */
int portscan(struct portscan_data *data, unsigned port);

/*
 * Allows changing the logging level (default: PORTSCAN_LOG_LEVEL_ERROR).
 *
 * Returns 0 on success
 */
int portscan_set_log_level(portscan_log_level_t level);

/*
 * Allows changing the time we wait for the sockets to connect.
 * Some devices may need longer than the default (1500ms).
 *
 * Returns 0 on success
 */
int portscan_set_sock_wait_tm(unsigned wait_ms);

