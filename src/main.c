/*
 * Copyright (C) 2024
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GateMeister GTK app for remote gate control
 */

#include <stdio.h>
#include <gtk/gtk.h>
#include <mosquitto.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <openssl/evp.h>
#include <modbus/modbus.h>
#include "portscan.h"

struct encrypt_block
{
	uint8_t  data[10];
	uint8_t  len;
	uint8_t  rand[3];
	uint16_t crc;
};

struct cmd_block
{
	uint8_t  open_req;
	uint8_t  close_req;
	uint8_t  pad1;
	uint8_t  state_open;
	uint16_t token;
	uint8_t  pad[4];
};

/* global variables */
int failed     = 0;
int connected  = 0;
int published  = 0;
int ctrl_token = 0;
int gate_open  = 0;
uint8_t key[16];

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t get_crc_16(uint8_t* buf, unsigned len, uint16_t poly, uint16_t init_val)
{
	uint16_t crc = init_val;

	for(unsigned i=0; i<len; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ poly;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Calculates the Modbus CRC for num_bytes of buf
 */
uint16_t get_modbus_crc(uint8_t* buf, unsigned num_bytes)
{
	return get_crc_16(buf, num_bytes, 0xA001, 0xFFFF);
}

/*
 * Encrypt the passed input_block
 * Returns 0 on success
 */
int encrypt_data(uint8_t *input, uint8_t *output, uint8_t *key)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	if ( ! ctx )
	{
		printf("%s: failed to alloc cipher ctx\n", __func__);
		return -1;
	}

	int status = -1;

	int success = EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);

	if ( ! success )
	{
		printf("%s: failed to init cipher\n", __func__);
		goto done;
	}

	int out_len;
	success = EVP_EncryptUpdate(ctx, output, &out_len, input, 16);

	if ( ! success )
	{
		printf("%s: failed to encrypt data\n", __func__);
		goto done;
	}

	status = 0;

done:
	EVP_CIPHER_CTX_free(ctx);

	return status;
}

/*
 * Decrypt the passed input block
 * Returns 0 on success
 */
int decrypt_data(uint8_t *input, uint8_t *output, uint8_t *key)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	if ( ! ctx )
	{
		printf("%s: failed to alloc cipher ctx\n", __func__);
		return -1;
	}

	int status = -1;

	int success = EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);
	if ( ! success )
	{
		printf("%s: failed to init cipher\n", __func__);
		goto done;
	}

	EVP_CIPHER_CTX_set_padding(ctx, 0);

	int out_len;
	success = EVP_DecryptUpdate(ctx, output, &out_len, input, 16);
	if ( ! success )
	{
		printf("%s: failed to decrypt data\n", __func__);
		goto done;
	}

	status = 0;

done:
	EVP_CIPHER_CTX_free(ctx);

	return status;
}

int encrypt_cb(struct cmd_block *cb, uint8_t *ciphertext, uint8_t *key)
{
	struct encrypt_block b = {0};

	memcpy(b.data, cb, sizeof(*cb));
	b.len = sizeof(*cb);
	b.rand[0] = random();
	b.rand[1] = random();
	b.rand[2] = random();

	uint16_t crc = get_modbus_crc((uint8_t*)&b, sizeof(b) - 2);
	b.crc = htons(crc);

	int status = encrypt_data((uint8_t*)&b, ciphertext, key);
	if ( status )
	{
		printf("%s encrypting failed\n", __func__);
		return -1;
	}

	return 0;
}

int decrypt_cb(struct cmd_block *cb, uint8_t *ciphertext, uint8_t *key)
{
	struct encrypt_block b = {0};

	int status = decrypt_data(ciphertext, (uint8_t*)&b, key);
	if ( status )
	{
		printf("%s: decryption failed\n", __func__);
		return -1;
	}

	if ( b.len != sizeof(struct cmd_block) )
	{
		printf("%s: error: bad length %d\n", __func__, b.len);
		return -1;
	}

	uint16_t crc = get_modbus_crc((uint8_t*)&b, sizeof(b) - 2);
	if ( crc != ntohs(b.crc) )
	{
		printf("%s: error: bad crc\n", __func__);
		return -1;
	}

	memcpy(cb, b.data, b.len);

	return 0;
}


/* Handle CONNACK message from broker */
void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
	(void) obj; /* unused */

	printf("%s: %s\n", __func__, mosquitto_connack_string(rc));
	if (rc)
	{
		/* prevent further connection attempts */
		mosquitto_disconnect(mosq);
		failed = 1;
		return;
	}

	printf("%s: subscribing\n", __func__);
	rc  = mosquitto_subscribe(mosq, NULL, "house/gate/state", 1);
	if (rc != MOSQ_ERR_SUCCESS)
	{
		fprintf(stderr, "Error subscribing: %s\n", mosquitto_strerror(rc));
		mosquitto_disconnect(mosq);
		failed = 1;
		return;
	}

	connected = 1;
}

/* Handle broker response to subscribe message */
void on_subscribe(struct mosquitto *mosq, void *obj,
					int mid, int qos_count, const int *granted_qos)
{
	(void) obj; /* unused */
	(void) mid; /* unused */

	if ( qos_count == 1 && granted_qos[0] <= 2 )
	{
		printf("%s: subscribed\n", __func__);
	}
	else
	{
		fprintf(stderr, "error: failed to subscribe\n");
		mosquitto_disconnect(mosq);
		failed = 1;
	}
}

/* Handle message from controller */
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg)
{
	(void) mosq; /* unused */
	(void) obj;  /* unused */

	uint8_t *buf = msg->payload;

	printf("%s: 0x", __func__);
	for(int i=0; i<msg->payloadlen; i++)
		printf("%02X", buf[i]);
	printf("\n");

	if ( msg->payloadlen == 16 )
	{
		struct cmd_block cb = {0};
		int error = decrypt_cb(&cb, buf, key);
		if ( error )
		{
			printf("%s: error: failed to decrypt cmd with error %d\n", __func__, error);
		}
		else
		{
			ctrl_token = ntohs(cb.token);
			gate_open  = cb.state_open;
		}
	}
	else
	{
		printf("%s: error: unexpected message size: %d\n",
					__func__, msg->payloadlen);
	}
}

/* Handle successful message publish */
void on_publish(struct mosquitto *mosq, void *obj, int msg_id)
{
	(void) mosq;   /* unused */
	(void) obj;    /* unused */
	(void) msg_id; /* unused */

	printf("%s: published\n", __func__);

	published = 1;
}

uint64_t millis()
{
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec * 1000LL + t.tv_usec / 1000;
}

void sleep_ms(unsigned count)
{
	struct timespec rem = {0};
	struct timespec req = { count / 1000, (count % 1000) * 1000 * 1000 };
	nanosleep(&req , &rem);
}

static void on_button_clicked(GtkWidget *button, gpointer data)
{
	gtk_widget_set_sensitive(button, false);

	const char *topic = "house/gate/cmd";

	ctrl_token = 0;

	struct cmd_block cb = {0};

	char *cmd = data;
	if ( memcmp(cmd,"open",4) == 0 )
	{
		cb.open_req = 1;
	}
	else if ( memcmp(cmd,"close",5) == 0 )
	{
		cb.close_req = 1;
	}
	else
	{
		printf("%s bad cmd %s\n", __func__, cmd);
		goto done;
	}

	printf("\n");
	printf("%s: connecting\n", __func__);

	mosquitto_lib_init();

	struct mosquitto *mosq = mosquitto_new(NULL, true, NULL);
	if(mosq == NULL)
	{
		fprintf(stderr, "Error: Out of memory.\n");
		goto cleanup;
	}

	mosquitto_connect_callback_set(mosq, on_connect);
	mosquitto_publish_callback_set(mosq, on_publish);
	mosquitto_subscribe_callback_set(mosq, on_subscribe);
	mosquitto_message_callback_set(mosq, on_message);

	int status = mosquitto_connect(mosq, "test.mosquitto.org", 1883, 10);
	if (status != MOSQ_ERR_SUCCESS)
	{
		fprintf(stderr, "Error: %s\n", mosquitto_strerror(status));
		goto destroy;
	}

	uint64_t t = millis();
	while ( millis() - t < 3000 )
	{
		mosquitto_loop(mosq, 100, 1);
		if ( connected || failed )
			break;
	}

	if ( failed )
	{
		printf("connection failed\n");
		goto destroy;
	}

	if ( ! connected )
	{
		printf("connection timeout\n");
		goto disconnect;
	}

	printf("%s: waiting for controller token\n", __func__);
	t = millis();
	while ( millis() - t < 3000 )
	{
		mosquitto_loop(mosq, 100, 1);
		if ( ctrl_token )
			break;
	}

	if ( ! ctrl_token )
	{
		printf("ERROR: Failed to receive token\n");
		goto disconnect;
	}

	printf("%s: got token 0x%04X, publishing %s/%s\n",
				__func__, ctrl_token, topic, cmd);

	cb.token = htons(ctrl_token);

	uint8_t ciphertext[16];
	status = encrypt_cb(&cb, ciphertext, key);
	if ( status )
	{
		printf("%s encrypting failed\n", __func__);
		goto disconnect;
	}

	status = mosquitto_publish(
				mosq, NULL, topic, sizeof(ciphertext), ciphertext, 2, false);
	if (status != MOSQ_ERR_SUCCESS)
	{
		fprintf(stderr, "Error publishing: %s\n", mosquitto_strerror(status));
		goto disconnect;
	}

	printf("%s: waiting for publish completion\n", __func__);
	t = millis();
	while ( millis() - t < 3000 )
	{
		mosquitto_loop(mosq, 100, 1);
		if ( published )
			break;
	}

	if ( ! published )
		printf("TIMEOUT\n");

disconnect:
	mosquitto_disconnect(mosq);
destroy:
	mosquitto_destroy(mosq);
cleanup:
	mosquitto_lib_cleanup();
	published = 0;
	connected = 0;
	failed    = 0;
	printf("%s: done\n", __func__);
done:
	gtk_widget_set_sensitive(button, true);
}

/*
 * Returns the number of read registers on success
 */
int read_modbus_registers(char* p_addr, unsigned port, uint16_t offset, uint16_t *val, uint16_t count)
{
	modbus_t* mb = modbus_new_tcp(p_addr, port);

	if( ! mb )
	{
		printf("Failed to alloc TCP object\n");
		return -1;
	}

	int status = modbus_connect(mb);

	if( status == 0 )
	{
		status = modbus_read_registers(mb, offset, count, val);

		if( status == -1 )
			printf("FAILED TO READ REGISTER!!\n");

		modbus_close(mb);
	}
	else
	{
		printf("Failed to open TCP connection\n");
	}

	modbus_free(mb);

	return status;
}

/*
 * Try to detect controller ip address on local network
 *
 * Returns 0 on success
 */
int find_controller_addr(char *p_addr_controller)
{
	struct portscan_data port_data = {0};

	/* Ethernet on the Arduino Opta is slow */
	portscan_set_sock_wait_tm(4000);

	/* find all local ip addresses with an open port */
	int status = portscan(&port_data, 502);
	if ( status )
	{
		printf("%s portscan returned error %d\n", __func__, status);
		return -1;
	}

	/* records the first address where we read the expected project id */
	status = -1;
	int addr_detected = 0;
	for(int i=0; i<256; i++)
	{
		if ( port_data.sock_fds[i] >= 0 )
		{
			close( port_data.sock_fds[i] );

			if ( ! addr_detected )
			{
				char tmp_addr[20];
				sprintf(tmp_addr, "%s%d", port_data.ip_range, i);

				uint16_t val = 0;
				uint16_t offset = 0; /* project id reg in GateMeisterController project */
				read_modbus_registers(tmp_addr, 502, offset, &val, 1);

				if ( val == 0x7733 )
				{
					sprintf(p_addr_controller, "%s%d", port_data.ip_range, i);
					addr_detected = 1;
					status = 0;
				}
			}
		}
	}

	return status;
}

static void on_pair_button_clicked(GtkWidget *button, gpointer data)
{
	(void) button; /* unused */
	(void) data;   /* unused */

	char ctrl_addr[16] = {0};

	int status = find_controller_addr(ctrl_addr);
	if ( status )
	{
		fprintf(stderr, "Failed to find controller\n");
		return;
	}

	printf("Found controller with addr %s\n", ctrl_addr);

	/* try to fetch the key */
	uint16_t *buf = (uint16_t *) key;
	status = read_modbus_registers(ctrl_addr, 502, 1, buf, 8);

	if ( status == 8 )
	{
		printf("Got key 0x");
		for(int i=0; i<16; i++)
			printf("%02X", key[i]);
		printf("\n");
	}
	else
	{
		printf("Failed to fetch key with error %d\n", status);
	}
}

void create_window(GtkApplication *app)
{
	GtkWidget *window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "GateMeister");
	gtk_window_set_default_size (GTK_WINDOW (window), 400, 730);

	GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);
	gtk_window_set_child (GTK_WINDOW (window), box);

	GtkWidget *button;

	button = gtk_button_new_with_label ("Pair");
	g_signal_connect (button, "clicked", G_CALLBACK (on_pair_button_clicked), NULL);
	gtk_box_append ( (GtkBox*)box, button );

	button = gtk_button_new_with_label ("Open");
	g_signal_connect (button, "clicked", G_CALLBACK (on_button_clicked), "open");
	gtk_box_append ( (GtkBox*)box, button );

	button = gtk_button_new_with_label ("Close");
	g_signal_connect (button, "clicked", G_CALLBACK (on_button_clicked), "close");
	gtk_box_append ( (GtkBox*)box, button );

	gtk_widget_set_visible (window, true);
}

static void activate (GtkApplication *app, gpointer user_data)
{
	(void) user_data; /* unused */

	GList *list = gtk_application_get_windows (app);

	if (list)
		gtk_window_present (GTK_WINDOW (list->data));
	else
		create_window(app);
}

int main(int argc, char *argv[])
{
	GtkApplication* app = gtk_application_new (
                             "org.shiftee.gatemeister", G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	int status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}

