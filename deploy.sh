#!/bin/sh
#
# Upload code to device, build it, and install it
#

USER=root
ADDR=$1

PROJ=gatemeister

[ -z "$ADDR" ] && ADDR=mobian.local

rm -rf build

echo "copying files to target..."
rsync --timeout=2 -a . ${USER}@${ADDR}:/dev/shm/$PROJ

[ $? -ne 0 ] && echo "cannot connect to target" && exit 1

ssh $SSH_OPTS $USER@$ADDR "
	cd /dev/shm/$PROJ

	meson setup build
	cd build
	meson compile
	meson install
"

